package com.example.jblshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JblshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(JblshopApplication.class, args);
    }

}
