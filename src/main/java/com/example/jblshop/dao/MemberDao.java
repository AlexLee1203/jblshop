package com.example.jblshop.dao;

import com.example.jblshop.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface MemberDao extends JpaRepository<Member, Integer>, JpaSpecificationExecutor<Member> {
    Member findByAccount(String account);
    Member findByAccountAndPassword(String account, String password );
    String findAccountByPassword(String password);

}




