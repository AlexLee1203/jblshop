package com.example.jblshop.api;

import com.example.jblshop.dao.MemberDao;
import com.example.jblshop.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MemberCreate {
    @Autowired
    private MemberDao memberDao;

    @PostMapping(value = "memberCreate")
    private String memberCreate(@RequestParam("account") String account, @RequestParam("name") String name, @RequestParam("age") Integer age, @RequestParam("pay") Integer pay, @RequestParam("burnPlace") String burnPlace,
                                Model model) {
        Member member = new Member();
        member.setAccount(account);
        member.setName(name);
        member.setAge(age);
        member.setPay(pay);
        member.setBurnPlace(burnPlace);
        memberDao.save(member);

        List<Member> memberList = memberDao.findAll();
        model.addAttribute("memberList", memberList);
        return "memberCenter";
    }

}

