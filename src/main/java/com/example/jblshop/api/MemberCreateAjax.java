package com.example.jblshop.api;

import com.example.jblshop.dao.MemberDao;
import com.example.jblshop.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.Operation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class MemberCreateAjax {
    @Autowired
    private MemberDao memberDao;

    @GetMapping(value = "/member/{id}")
    private Member memberDeatil(@PathVariable Integer id) {

        Member member = memberDao.findById(id).get();
        return member;
    }

    @GetMapping(value = "/member")
    private Member memberById(@RequestParam Integer id) {

        Member member = memberDao.findById(id).get();
        return member;
    }

    @PutMapping(value = "/member")
    private void memberUpdate(@RequestBody Member memberReq) {

        Member member = memberDao.findByAccount(memberReq.getAccount());
//        Member member=memberDao.findByAccount(account);
//        member.setAccount(memberReq.getAccount());
        member.setName(memberReq.getName());
        member.setAge(memberReq.getAge());
        member.setPay(memberReq.getPay());
        member.setBurnPlace(memberReq.getBurnPlace());
        memberDao.save(member);
    }

    @DeleteMapping(value = "/member/{id}")
    private void memberDelete(@PathVariable Integer id) {
        memberDao.deleteById(id);
    }

    @PostMapping(value = "/member")
//    private void memberCreate(@RequestParam("account") String account, @RequestParam("name") String name, @RequestParam("age") Integer age, @RequestParam("pay") Integer pay, @RequestParam("burnPlace") String burnPlace,
//                              Model model) {
    private Member member(@RequestBody Member memberReq) {
        Member member = new Member();
        member.setAccount(memberReq.getAccount());
        member.setName(memberReq.getName());
        member.setAge(memberReq.getAge());
        member.setPay(memberReq.getPay());
        member.setBurnPlace(member.getBurnPlace());
        memberDao.save(member);

        return member;
//        List<Member> memberList = memberDao.findAll();
//        model.addAttribute("memberList", memberList);
//        return "memberCenter";
    }
}

