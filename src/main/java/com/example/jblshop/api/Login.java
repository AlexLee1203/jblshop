package com.example.jblshop.api;

import com.example.jblshop.dao.MemberDao;
import com.example.jblshop.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class Login {
    @Autowired
    private MemberDao memberDao;

    @PostMapping(value = "login")
    private String login(@RequestParam("account") String account, @RequestParam("password") String password,
                         Model model) {
        Member member = memberDao.findByAccount(account);
        if (member != null && member.getPassword().equals(password)) {
            List<Member> memberList = memberDao.findAll();
            model.addAttribute("memberList", memberList);
            return "memberCenter";
        } else {
            model.addAttribute("err", "password error");
            return "index";
        }
    }
}
