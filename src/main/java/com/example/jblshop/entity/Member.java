package com.example.jblshop.entity;

import javax.persistence.*;

@Entity
public class Member {
    public Member() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String account;
    private String password;
    private String name;
    private Integer age;
    private Integer pay;
    private String burnPlace;
    @ManyToOne(targetEntity = Department.class)
    private Department dep;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getPay() {
        return pay;
    }

    public void setPay(Integer pay) {
        this.pay = pay;
    }

    public String getBurnPlace() {
        return burnPlace;
    }

    public void setBurnPlace(String burnPlace) {
        this.burnPlace = burnPlace;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
